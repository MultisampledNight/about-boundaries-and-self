# About boundaries and self

See the [typst](https://typst.app) file in this repo.
You can compile it to a nice PDF via

1. Installing [typst], the document engine used here
2. Installing [flow], the note template used here
3. `typst compile boundaries-and-self.typ`

Alternatively, you can also just check out the latest release.

# License

CC BY-NC-SA 4.0. See [LICENSE.md](./LICENSE.md).

Non-legally-binding:
If you do create derivatives of this work,
consider adjusting
    names and
    versions
accordingly
so they fit *you*.

[typst]: https://github.com/typst/typst?tab=readme-ov-file#installation
[flow]: https://github.com/MultisampledNight/flow
