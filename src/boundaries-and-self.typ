#import "@local/flow:0.1.0": *
#show: note.with(
  title: "Boundaries and self",
  author: "MultisampledNight",
  version: "0.4.2",
  cw: (
    "\"you\" at reader",
    "medical language",
    "medicine",
    "blood",
    "needle",
    "hrt",
    "controlling behavior",
  ),

  license: (
    name: "CC BY-NC-SA 4.0",
    link: "https://creativecommons.org/licenses/by-nc-sa/4.0/",
  ),
  copyright: 2024,

  keywords: gradient-map(
    (
      "responsible",
      "soft",
      "consent",
      "guilt",
      "effect",
      "intent",
      "hard",
      "emergency",
    ),
    duality.values().slice(2, -1)
  ) + (
    oximeter: <oximeter>,
    "constructive feedback": <con-fb>,
    "destructive feedback": <des-fb>,
  ) + (
    "peripheral",
    "fine",
    "must",
    "not",
    "explicit",
    "voluntary",
    "informed",
    "limited",
    "revoke",
    "capable",
    "reward",
  ).map(kw => (kw, strong)).to-dict(),
)

#show "SpO2": _ => {
  [%SpO]
  sub(typographic: false)[2]
}
#show heading.where(
  level: 1,
  outlined: true,
): it => pagebreak(weak: true) + it


#let repo = "https://gitlab.com/MultisampledNight/boundaries-and-self/"
#let issues = repo + "-/issues"
#let except = hint
#let clarify = corollary


= How to read this

This intends to clarify
what I want interactions with me
to look like.
It is very likely imperfect though:
#link(issues)[
Please do share
any constructive feedback
that's in your mind!
]

*Please read the content warnings.*
If anything could be triggering,
please let me know what warnings exactly and
I'll send a modified version of this
to you.

== Regarding importance

Most likely, @boundaries
will suffice for you.
@mistakes and @glossary
are included for understanding the context and
allow reading further into the details.
@me are just there
if you happen to want to watch out for me,
which is probably not your obligation.

== Environments

A colored line to the left
with an icon in the same color
denotes
that the contained part has a special meaning:

#box({
  except[
    Explicit exceptions
    from the base boundary.
  ]
  clarify[
    Clarifications that
    further explain something.
  ]
  caution[
    Extremely important things.
  ]
  define[
    Short definitions applicable for this document only.
  ]
})


= Boundaries <boundaries>

#caution[
  These are the default boundaries
  I have with most entities.
  However, I might still
  set more beyond these
  on an individual basis.
  I will always communicate this explicitly.
]

== Hard

#define[
*Hard boundaries*
always apply,
no matter what
(except for their listed exceptions
as marked with
#(gfx.markers.at("o").icon)(invert: false)
).
]

=== Needles

- Do not show them or
  reproductions of them
  to me.

- Do not let me feel them.

- Do not describe *how* they are used.

#clarify[
  Talking about what
  they are used *for*
  or which situations in
  is fine.

  - This also includes medical or HRT usage.
  - It _can_ make me uncomfortable,
    but it's usually bearable.
]

#except[
  In emergencies,
  showing them to me is fine.
]

=== Control

- Do not limit my mental freedom.
- Do not track my location continuously.

=== Memory

- Do not destroy my knowledge.
- Do not modify my memories.

#clarify[
  This includes both
  physical and
  psychological
  representations.
]

=== Feedback

- Do not give destructive feedback
  on my projects.

=== Routines

- In habitual contexts,
  do not repeatedly inhibit
  my routines.

== Soft

#define[
  *Soft boundaries* may be
  selectively overriden by my consent
  (see @consent).
]

=== Touch

- Do not touch me.
- Do not breathe closely onto my neck.

#except[
  Touch is fine if I am touching you already.
]

#caution[
  Do not hit me under any circumstances.
]

=== Objects

- Do not destroy objects under my ownership.

=== Compliments

- Do not give me compliments.

#except[
  Unless they are bound
  to a _specific_ work,
  mechanism or
  pattern,
  or sufficient reasoning is provided.
]

#clarify[
  - For example, "I like your style"
    is too vague.
    - What exact part of my style
      is interesting to you?
    - What did you notice of it?

  - Good: "Your styling is consistently thought-out."
    - Has a specific aspect that it is about.

  - Extremely good:
    "I like the color combination
    of the top with the pants.
    They contrast each other well."

  - Extremely good:
    "That algorithm is elegant.
    It expresses its intent
    in a concise  way."
]

#clarify[
  The reasoning behind this
  is my easily overboostable
  self-confidence.
  I am also quite lax with enforcing this.
]

=== Personally Identifiable Information

- Do not take pictures of me.
- Do not make voice recordings of me.
- Do not share details
  you know to be confidential
  with others.

#except[
  Sharing confidential details with entities
  who are in duty of confidentiality is fine.
]

=== Surprises

- Do not perform surprises or give me gifts.

#except[
  Explicitly ask me first and
  tell me in detail what you have planned.
]

#clarify[
  This might seem ridiculous,
  but it is entirely honest.
  Me wanting to know _what's_ happening first
  can still render me thankful
  for a gift
  if I accept it.
]


= If things do go wrong <mistakes>

== Emergencies

#define[
  Here, an acute situation
  with grave
  physical and/or
  psychological
  harm imminent,
  affecting one or multiple entities.
]

- Short-term emergencies override
  most soft boundaries temporarily.
  - The only excluded one is sharing of PII.
  - I'll probably need aftercare though.

- I always carry an oximeter
  in my backpack or
  on me.
  - *Ask me before using* if I am present.
    - Maybe I need to disinfect it or
      need it otherwise.
  - If I am not present,
    you are free to use it for an emergency.

== In interactions

=== Meta-info

- This is agnostic to
  the entities affected.
  - It is what I wish for others to do
    when they've hurt me.
  - It is roughly what I do
    when I've hurt others.

- Mistakes will necessarily happen.
  - Regardless of your intentions.
  - Even I make mistakes all the time.
  - They are okay,
    as long as one takes responsibility
    for caused effects.

- Interactions are *difficult*.
  - They're worth it though.
    They can help you understand the world.
  - But without post-processing,
    you're stuck in a maze
    without a plan,
    metaphorically.

=== Procedure

You don't have to follow
this exact procedure.
Nor will it be always applicable.
Adjust as you see fit and
be reasonable about it.

+ Remove *imminent* danger
  as soon as possible.

  - Considering
    circumstances,
    context and
    ambiguity.

  - You might not know
    everything that's happening.

  - *Seek out help*
    if you're even slightly unsure.
    - E.g. if on an event and an overload
      is happening,
      consider asking
      for a quiet dark non-distracting place.

+ Take responsibility for
  what effect you caused.

  - In my opinion,
    responsibility != guilt.
    - Guilt usually implies
      responsibility.
    - However,
      one can be responsible
      without being guilty.

  - *Intent is not magic* and
    does not justify everything.
    @intent-is-not-magic

+ Attempt to *learn* from it.
  - Are there factors
    ahead of the main harmful interactions
    that influenced you?
    - For example,
      medicine or
      previous heated interactions.

  - Which *actors* were at play?
    - What were their motivations?

  - At which *points*
    could you have done
    something *differently*?
    - Was that *realistically* possible?
    - What *effect* would that have had?
    - Why or why not?

+ Offer to communicate
  with the victim about it.

  - At a time and place controlled by them.

  - Ideally with:
    #set enum(numbering: "a)")
    + A mediator trusted by both.
      - Who hasn't partaken
        in the harmful interaction.
    + *Concrete* points to talk about.
    + Cooperativity.
    + Desire to reach a satisfying resolve.

  - Let the victim state their perspective
    without interruption.

  - Shutting down comms
    from your side
    is not helpful
    if you'd be cooperative.

  - There can also be multiple
    post-processing communication sessions.

  - You also don't have to be alone there,
    nor has the victim to be.
    - Ask the victim if they're fine with
      you bringing someone though.

+ Respect wishes
  and further boundaries set by the victim.


= Various things about me <me>

- I am regularly tested for STDs,
  once each quarter.

== Numerical needs <needs>

Each day:

#let needs(
  cols: ("minimum", "optimal"),
  ..it,
) = table(
  columns: 4,
  inset: 0.75em,
  align: (right,) + (left,) * (cols.len() + 1),
  fill: (_, y) => if calc.rem(y, 2) == 0 {
    gamut.sample(7.5%)
  } else {
    bg
  },
  table.header(
    [need],
    ..cols.map(strong),
    [unit],
  ),
  ..it
    .named()
    .pairs()
    .map(((name, (unit, ..values))) => {
      (name,)
      values.map(x => [#x])
      ([#unit],)
    })
    .join()
)

#let no = (none, table.cell(
  colspan: 2,
  align: center,
  dim[_confidential_],
))

#needs(
  nutrition: ("kcal", 1750, 2100),
  water-intake: ("l", 0.75, 2),
  sleep: ("h", 6, 8),
  min-time-alone: ("min", 10, 45),
  fresh-air: ("min", 5, 20),
  medicine: no,
)

= Glossary <glossary>

== Consent <consent>

#define[
  Given entities $x, y$ and an action $a$:
  $x$ *consenting*
    to $a$
    towards $y$
  is agreement
    done by $x$
    towards $y$
    to do $a$
  which is:

  / Explicit:
    $x$ comunicates a signal
    that can be interpreted unambiguously
    as affirmation to do $a$ with $y$.

  / Voluntary:
    $x$ must not be,
    directly or indirectly,
    pushed into agreeing.

  / Informed:
    $x$ is fully aware
    of
    - *what, when, how*
      and *with whom*
      $a$ happens
      - Or freely accepts
        explicit ambiguity.
    - failure modes
    - {short,long}-term effects

  / Limited:
    $a$ and its effects
    last for a
    defined and
    communicated
    amount of time.
    No other action is agreed to.

  / Revokable:
    $x$ must be able to retract their agreement
    at any point before or during $a$.

  // FIXME: This is actually pretty hard to define.
  // Hmm.
  / Capable:
    $x$ must be psychologically and medically
    able to make a free choice.
]

- Potentially useful acronym: *EVILRC*
  (#fxfirst("Explicit Voluntary Informed Limited Revokable Capable"))

#clarify[
  Consent doesn't have to be given verbally.
  It can also, for example,
  be given via tactile or visual inputs,
  as long as it's still unambiguous.
]

#clarify[
  It's easier to ensure voluntariness
  by using language that explicitly offers an alternative option. For example:

  - Bad: "We should do $a$."
    (no choice given)
  - Better: "Do you want to do $a$?"
    (explicit question)
]

#caution[
  Especially capability can be lowered
  quite easily accidentally!
  Power structures,
  knowledge differences,
  drug influences,
  repeated asking,
  general mental health
  and rewards
  can influence this very gravely.
]

== Smaller things

This is to the best of my knowledge
and hence unlikely
to be completely accurate.
Nor is this medical advice,
*seek out a professional
if you're unsure*.

/ Oximeter:
  Small device with a display
  that can be
  non-intrusively attached
  to a finger.
  - Measured vital parameters:
    - *SpO2*
    - Heartbeats per minute
  <oximeter>

/ SpO2:
  How much oxygen is in blood.

  - Displayed as percentage between
    - 0%, no oxygen whatsoever
      - Impossible for an alive organic being.
    - 100%, blood is fully saturated with oxygen, normal.

  - Measures at the peripherals.
    - It isn't always accurate,
      but usually a very good hint.

  - It can fall into 3 categories:
    - 95% to 100% is fine.
    - 90% to 95% is worrisome,
      seek medical attention.
    - $<=$ 90% is dangerous, *call an ambulance*.

  - To raise it:
    - Open the windows and
      let the patient get some fresh air.
    - Let the patient breathe deeply.
      - You breathing with them
        helps in coordination and trust.

/ Heartbeats per minute:
  How often the heart beats in one minute.

  #caution[
    What's healthy
    is highly
    {individual,context}-dependent.
  ]

  - Usually taken from a few beats and
    then extrapolated.

  - _Often_:
    - The higher, the more excited.
    - The lower, the more relaxed.
    - Below 60 or above 140 is worrisome.
    - 60 while asleep or
      just waking up
      can be normal.
    - 140 when one has just ran
      a distance can also be normal.
      - It should calm down after that though.


/ Peripherals:
  Extremities,
  limbs,
  fingers,
  the works.

/ CNC: #fxfirst("Consensual Non Consent").
  Consent that is negotiated
  to a usually broader timeframe,
  with revokal mechanisms
  restricted to an agreed-upon
  set of communication.

/ Constructive feedback:
  Feedback that directly
  suggests actions to change things
  for the better
  possible.
  <con-fb>

/ Destructive feedback:
  Feedback that does not allow
  to be acted upon.
  Opposite of constructive feedback.
  <des-fb>

#bibliography("ref.yml")
